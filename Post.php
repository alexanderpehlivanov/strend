<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <title>Post</title>
     <link rel="icon" 
      type="png" 
      href="img/minilogo.png"
      />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/indexheadstyle.css" rel="stylesheet" type="text/css">
    <link href="css/poststyle.css" rel="stylesheet" type="text/css">
</head>
    <body>
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="indexCOPY.php"><img alt="Brand" src="img/minilogo2.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="divider"><a href="index.php">Home <span class="sr-only">(current)</span></a></li>
        <li class="active"><a href="Post.php">Post</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <li><a href="Login.php">Login</a></li>
        <li><a href="Register.php">Register</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container">
  <div class="jumbotron">
    <ul class="media-list">
  <li class="media">
    <div class="media-left">
      <a>
        <img class="media-object" src="img/profilpicture.png" alt="...">
      </a>
    </div>
    <div class="media-right">
      <h4 id="Name" class="media-heading"><b>Name:</b>  Alexander Mitko Pehlivanov</h4>
      <h4 id="Country" class="media-heading"><b>Country:</b>  Bulgaria,Burgas</h4>
      <h4 id="Number" class="media-heading"><b>Number:</b>  0872152365</h4>
      <h4 id="Date" class="media-heading"><b>Date:</b>  29.04.2015</h4>
    </div>
  </li>
</ul>
  <p>
  "Confabulation Theory" is not a textbook but a collection of papers by the author plus two talks on DVDs. The writing is very clear and the talks are excellent, taking you through the theory one step at a time. Hecht-Nielsen is a great teacher. But the papers are repetitive, covering the same basics over and over again, while skimming over the key topics of self-organization within modules and implementation of the winner-take-all networks. The book spends too much time on sentence confabulation experiments (which appear to be similar in approach to statistical language translation) and not enough on the applications sketched out in the last two chapters (vision and continuous speech segmentation).

Hecht-Nielsen has an unfortunate habit of overstatement which weakens his presentation. In the text and the videos he repeatedly points out how "starkly alien" confabulation theory is, and how it might revolutionize "psychology, education, philosophy, psychiatry, medicine (both human and veterinary), law, and theology." Really? If you're unfamiliar with neuroscience, the massive parallelism and nonlinear dynamics might seem alien, but this is old hat in the field. His "Fundamental Theorem of Cognition" goes over the edge, though, seemingly suggesting that he has "solved" the problem of cognition once and for all; the "fundamental theorem of confabulation" would have been a more appropriate and less annoying name.

There are a lot of interesting ideas here, and I finished the book wanting to know more. I hope that someday Hecht-Nielsen writes a book with greater depth in implementation issues and more breadth on applications. The papers making up the core of the theory are available on the Web, so the book is worth buying only for the convenience of having everything bound together.
</p>
  </div>
</div>
    </body>
</html>


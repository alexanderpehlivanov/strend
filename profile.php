<?php
include('php/session.php');
?>
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <title>Home</title>
     <link rel="icon" 
      type="png" 
      href="img/minilogo.png"
      />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/indexheadstyle.css" rel="stylesheet" type="text/css">
    <link href="css/loginstyle.css" rel="stylesheet" type="text/css">
</head>
    <body>
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="indexCOPY.php"><img alt="Brand" src="img/minilogo2.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="active"><a href="profile.php">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="Postlogin.php">Post</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <?php echo '<li><a class="navbar-brand"><img id="profile-img-login" class="profile-img-card-login" src="'.$pic['profilpicture'].'" /></a></li>' ?>
        <li><a href="UCP.php"><?php echo $login_session; ?></a></li>
        <li><a href="php/logout.php">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container">
  <div class="jumbotron">
    <h1>Hello and Welcome to SContract</h1>
    <p>Гугъл[2] (на английски Google Inc.) е американска Интернет и софтуерна корпорация, специализирана основно в търсенето в Интернет, но притежаваща подразделения за изчисления в облак, онлайн потребителски имейли (gmail), карти, сканиране и архивиране в дигитален вид на книги и много други, като нейната мисия, както е формулирана от компанията, е „да организира световната информация и да я направи универсално достъпна и полезна за всеки“.[3] Google е базирана в град Маунтин Вю, щат Калифорния. Към юни 2013 г. Google има постоянно заети 44 777 служители. Компанията разполага с хиляди сървъри по света, които обработват милиони заявки за търсене ежедневно и около 1 петабайт потребителски данни всеки час.

Компанията е създадена от американците Лари Пейдж и Сергей Брин (от руски произход), докато те са студенти в Станфордския университет и се опитват да измислят ефикасен алгоритъм (машина) за търсене на уеб страници по ключова дума. Фирмата е регистрирана като частна компания на 4 септември 1998 г. Оттогава Google продължава да се разраства непрекъснато чрез разработване на нови продукти, придобиване и партньорство с други компании. Основните си приходи Google получава от главната услуга, която предлага – Google Търсене – генериране на резултатите от търсене на текст в световната мрежа (търсачка). Сегашен главен изпълнителен директор е Лари Пейдж.

Доминиращата пазарна позиция на услугите, предлагани от Google води до критика към компанията, свързана с проблеми, включващи неприкосновеност и тайна на личната информация, авторски права и цензура.
</p> 
  </div>
  <div class="row">
  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="img/Agreement.jpg" alt="...">
      <div class="caption">
        <h3 id="Agreement">Agreement</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit
        Lorem ipsum dolor sit amet, consectetur adipisicing elit
        Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="img/Contract.jpg" alt="...">
      <div class="caption">
        <h3 id="Agreement">Contract</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit
        Lorem ipsum dolor sit amet, consectetur adipisicing elit
        Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
      </div>
    </div>
  </div>
   <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
      <img src="img/3rd side.jpg" alt="...">
      <div class="caption">
        <h3 id="Agreement">3rd Side</h3>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit
        Lorem ipsum dolor sit amet, consectetur adipisicing elit
        Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
      </div>
    </div>
  </div>
</div>
</div>
    </body>
</html>
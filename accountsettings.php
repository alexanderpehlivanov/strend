<?php
include('php/session.php');
?>
<!DOCTYPE html>
<html>
<html lang="en">
<head>
    <title><?php echo $login_session; ?></title>
     <link rel="icon" 
      type="png" 
      href="img/minilogo.png"
      />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-filestyle.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/indexheadstyle.css" rel="stylesheet" type="text/css">
    <link href="css/loginstyle.css" rel="stylesheet" type="text/css">
    <link href="css/UCP.css" rel="stylesheet" type="text/css">
    <script>$(":file").filestyle({input: false});</script>

</head>
    <body>
    <nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="indexCOPY.php"><img alt="Brand" src="img/minilogo2.png"></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="divider"><a href="profile.php">Home <span class="sr-only">(current)</span></a></li>
        <li><a href="Postlogin.php">Post</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form>
      <ul class="nav navbar-nav navbar-right">
        <?php echo '<li><a class="navbar-brand"><img id="profile-img-login" class="profile-img-card-login" src="'.$pic['profilpicture'].'" /></a></li>' ?>
        <li class="active"><a href="UCP.php"><?php echo $login_session; ?></a></li>
        <li><a href="php/logout.php">Logout</a></li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container">
    <div class="row profile">
    <div class="col-md-3">
      <div class="profile-sidebar">
        <!-- SIDEBAR USERPIC -->
        <div class="profile-userpic">
          <?php echo '<img src="'.$pic['profilpicture'].'" class="img-responsive" alt="">' ?>
        </div>
        <!-- END SIDEBAR USERPIC -->
        <!-- SIDEBAR USER TITLE -->
        <div class="profile-usertitle">
          <div class="profile-usertitle-name">
            <?php echo $login_session; ?>
          </div>
          <div class="profile-usertitle-job">
            Student
          </div>
        </div>
        <!-- END SIDEBAR USER TITLE -->
        <!-- SIDEBAR BUTTONS -->
        <!-- END SIDEBAR BUTTONS -->
        <!-- SIDEBAR MENU -->
        <div class="profile-usermenu">
          <ul class="nav">
            <li>
              <a href="UCP.php">
              <i class="glyphicon glyphicon-home"></i>
              About <?php echo $login_session; ?> </a>
            </li>
            <li class="active">
              <a href="accountsettings.php">
              <i class="glyphicon glyphicon-user"></i>
              Account Settings </a>
            </li>
            <li>
              <a href="#" target="_blank">
              <i class="glyphicon glyphicon-ok"></i>
              Tasks </a>
            </li>
            <li>
              <a href="#">
              <i class="glyphicon glyphicon-flag"></i>
              Help </a>
            </li>
          </ul>
        </div>
        <!-- END MENU -->
      </div>
    </div>
    <div class="col-md-9">
            <div class="profile-content">
            <form action="upload.php" method="post" 

            enctype="multipart/form-data"> 

            <div><label id="upload">Upload Profil Pictures:</label></div> 
            <div id="chosefile2"></div>
            <div> 
            <input type="file" class="filestyle" data-input="false" name="upload">
            <div id="chosefile"></div>
            <input type="submit" class="btn btn-default" value="Submit" name="submit" />
            </div> 
 

            </form>
            </div>
    </div>
  </div>
</div>
<br>
<br>
    </body>
</html>
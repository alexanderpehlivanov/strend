<!DOCTYPE html>
<html lang="en">
<head>
    <title>Home</title>
     <link rel="icon" 
      type="png" 
      href="img/minilogo.png"
      />
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="css/indexheadstyleCOPY.css" rel="stylesheet" type="text/css">
    <script type="text/javascript">
    function testverstionMobbile () {
      br++;
      var rotation = document.getElementById('TestMobbile');
      var navbar = document.getElementById('navigationbarMobbile');
      rotation.style.color = "#222";
      rotation.style.webkitTransition = "color 0.5s";
      navbar.style.top = "-320px";
      if(br == 2) {
        br = 0;
        rotation.style.webkitTransition = "color 0s";
        rotation.style.color = "white";
        navbar.style.top = "0px";
      }
    }
   function testverstion () {
      br++;
      var rotation = document.getElementById('Test');
      var navbar = document.getElementById('navigationbar');
      rotation.style.webkitTransform = "rotate(0deg)";
      navbar.style.top = "0px";
      navbar.style.webkitTransition = "top 0.5s";
      rotation.style.paddingTop = "90px";
      rotation.style.webkitTransition = "all 0.3s ease-in-out";
      if(br == 2) {
        br = 0;
        navbar.style.top = "-100px";
       rotation.style.paddingTop = "0px";
        rotation.style.webkitTransform = "rotate(90deg)";
      }
    }
    </script>
    <script type="text/javascript">
    function infofirst () {
      //Main_Border
      var border = document.getElementById('borderdiv');
      border.style.webkitTransition = "all 0.3s ease-in-out";
      border.style.right = "0%";
      //Information_Text
      var textFirst = document.getElementById("Second_Slide_Information_Test_First");
      var textSecond = document.getElementById("Second_Slide_Information_Test_Second");
      var textThird = document.getElementById("Second_Slide_Information_Test_Third");
      textFirst.style.opacity = "1";
      textFirst.style.fontSize = "34px";
      textFirst.style.width = "100%";
      textFirst.style.height = "200px";
      textSecond.style.opacity = "0";
      textSecond.style.fontSize = "0";
      textSecond.style.width = "0";
      textSecond.style.height = "0";
      textThird.style.opacity = "0";
      textThird.style.fontSize = "0";
      textThird.style.width = "0";
      textThird.style.height = "0";
      //Text_Border
      var textBorderFirst = document.getElementById('Second_Slide_First_Div_Text_Border');
      var textBorderSecond = document.getElementById('Second_Slide_Second_Div_Text_Border');
      var textBorderThird = document.getElementById('Second_Slide_Third_Div_Text_Border');
      textBorderFirst.style.width = "40px";
      textBorderSecond.style.width = "0px";
      textBorderThird.style.width = "0px";
    }
    function infosecond () {
      //Main_Border
      var border = document.getElementById('borderdiv');
      border.style.webkitTransition = "all 0.3s ease-in-out";
      border.style.right = "-33.33333333%";
      //Information_Text
      var textFirst = document.getElementById('Second_Slide_Information_Test_First');
      var textSecond = document.getElementById('Second_Slide_Information_Test_Second');
      var textThird = document.getElementById('Second_Slide_Information_Test_Third');
      textFirst.style.opacity = "0";
      textFirst.style.fontSize = "0";
      textFirst.style.width = "0";
      textFirst.style.height = "0";
      textSecond.style.opacity = "1";
      textSecond.style.fontSize = "34px";
      textSecond.style.width = "100%";
      textSecond.style.height = "200px";
      textThird.style.opacity = "0";
      textThird.style.fontSize = "0";
      textThird.style.width = "0";
      textThird.style.height = "0";
      //Text_Border
      var textBorderFirst = document.getElementById('Second_Slide_First_Div_Text_Border');
      var textBorderSecond = document.getElementById('Second_Slide_Second_Div_Text_Border');
      var textBorderThird = document.getElementById('Second_Slide_Third_Div_Text_Border');
      textBorderFirst.style.width = "0px";
      textBorderSecond.style.width = "40px";
      textBorderThird.style.width = "0px";
    }
    function infothird () {
      //Main_Border
      var border = document.getElementById('borderdiv');
      border.style.webkitTransition = "all 0.3s ease-in-out";
      border.style.right = "-66.66666666%";
      //Information_Text
      var textFirst = document.getElementById('Second_Slide_Information_Test_First');
      var textSecond = document.getElementById('Second_Slide_Information_Test_Second');
      var textThird = document.getElementById('Second_Slide_Information_Test_Third');
      textFirst.style.opacity = "0";
      textFirst.style.fontSize = "0";
      textFirst.style.width = "0";
      textFirst.style.height = "0";
      textSecond.style.opacity = "0";
      textSecond.style.fontSize = "0";
      textSecond.style.width = "0";
      textSecond.style.height = "0";
      textThird.style.opacity = "1";
      textThird.style.fontSize = "34px";
      textThird.style.width = "100%";
      textThird.style.height = "200px";
      //Text_Border
      var textBorderFirst = document.getElementById('Second_Slide_First_Div_Text_Border');
      var textBorderSecond = document.getElementById('Second_Slide_Second_Div_Text_Border');
      var textBorderThird = document.getElementById('Second_Slide_Third_Div_Text_Border');
      textBorderFirst.style.width = "0px";
      textBorderSecond.style.width = "0px";
      textBorderThird.style.width = "40px";  
    }
    </script>
    <script type="text/javascript">
    $(document).ready(function(){

    $('#dialog-message').dialog({
        modal: true,
        autoOpen: false,
        buttons: {
            Ok: function() {
                $(this).dialog("close");
            }
        }
    });



    $('#button').on('click', function() {
        var name = $('#name').val();
        var email = $('#email').val();
        var message = $('#message').val();
        var dataString = 'name=' + name + '&email=' + email + '&subject=' + subject + '&message=' + message;
        $.ajax({
            type: "POST",
            url: "php/ContactUs.php",
            data: dataString,
            success: function() {
                window.alert("Qj mi gaza");
                $('#dialog-message').dialog('open');
            }
        });
    });
});​
    </script>
</head>

    <body>
        <!--Mobbile-->
        <header class="IcondivMobbile">
        <div id="navigationbarMobbile">
        <nav>
        <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="Post.php">Post</a></li>
        <li><a href="Login.php">Login</a></li>
        <li><a href="Register.php">Register</a></li>
        </ul>
        </nav>
        </div>
        <div id="iconnavigationbardivMobbile">
        <span id="TestMobbile" class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" onclick='testverstionMobbile()'>
        </span>
        </div>
        </header>
        <!--End-Mobbile-->
        <header class="Icondiv">
        <div id="navigationbar">
        <nav>
        <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="Post.php">Post</a></li>
        <li><a href="Login.php">Login</a></li>
        <li><a href="Register.php">Register</a></li>
        </ul>
        </nav>
        </div>
        <div id="iconnavigationbardiv">
        <span id="Test" class="glyphicon glyphicon-menu-hamburger" aria-hidden="true" onclick='testverstion()'>
        </span>
        </div>
        </header>
        <div class="First">
        <div class="First_Slide_First_Div">
        <img src="img/minilogo.png">
        <h1>SCONTRACT</h1>
        <p>We make your life easier</p>
        <div class="First_Slide_First_Div_Border"></div>
        </div>
        </div>
        <div class="SecondSlide">
        
        <div id="Help">
        <div id="helpdiv">
          <p>About Site</p>
          </div>
        </div>

        <div id="Second">
        
        <div id="Second_Slide_First_Div" onclick='infofirst()'>
          <div id="Second_Slide_First_Div_Text"><p>Agreement</p><div id="Second_Slide_First_Div_Text_Border"></div></div>
        </div>
        
        <div id="Second_Slide_Second_Div" onclick='infosecond()'>
          <div id="Second_Slide_Second_Div_Text"><p>Contract</p><div id="Second_Slide_Second_Div_Text_Border"></div></div>
        </div>
        
        <div id="Second_Slide_Third_Div" onclick='infothird()'>
          <div id="Second_Slide_Third_Div_Text"><p>3rd Side</p><div id="Second_Slide_Third_Div_Text_Border"></div></div>
        </div>
        
        <div id="borderdiv">
          
        </div>

        <div id="Second_Slide_Information_Div">

        <div id="Second_Slide_Information_Test_First">
        <p>Agrement is the easiest way.We make every one</p>
        </div>

        <div id="Second_Slide_Information_Test_Second">
        <p>We make contracts between clients and other mans</p>
        </div>

        <div id="Second_Slide_Information_Test_Third">
        <p>We are 3rd Side of contract</p>
        </div>

        </div>

        </div>
        
        </div>
        <div id="Third" class="container">
        
        <div id="FunctionInformation">
          <p id="AboutFunctions">About Functions</p>
          <div id="AboutFunctionsBorder"></div>
          <p id="AboutFunctionsInformation">This web-site is created for:<br>1.Second Chance For students , who dont have money to pay their educationand and here reposition our website. We give you chance to make you dream real, you only have to register yourself in this web-site and fill the information about you then you have to post requst about which university you want to go and how much is the fees.if someone company approve you will make contract with them<br>2.Do you have job? If answer is 'no' here is our web-site.It is simple you have to only register yourself , fill the informatin and post requst in the jobs section.We offer you the easiest way and the fastest way to get you job.</p>
          </div>
        </div>
        <div id="Fourth">
        <p>Make Your Dreams Real</p>
        </div>
        <div id="Fifth" class="container">
          <p id="TeamUs">Team</p>
          <div id="TeamUsBorder"></div>
          <div class="row" id="TeamUsInformation">
          <div class="col-sm-6 col-md-4">

          <div id="Ivo"><img src="img/Ivo.jpg" class="TeamUsImages"><p class="Names">Ivo</p>
          <p class="Developer">Developer:<em> < Frond-End and Back-End Developer /> </em></p>
          <p class="SomethingAboutMe">About Me</p>
          <div class="SomethingAboutMeBorder"></div>
          <p class="AboutMe"></p>
          </div>

          </div>

          <div class="col-sm-6 col-md-4">

          <div id="Alex"><img src="img/Alex.jpg" class="TeamUsImages"><p class="Names">Alex</p>
          <p class="Developer">Developer:<em> < Frond-End and Back-End Developer /> </em></p>
          <p class="SomethingAboutMe">About Me</p>
          <div class="SomethingAboutMeBorder"></div>
          <p class="AboutMe">“ I follow my dreams And consequence of this now I am web-developer and I love making beautiful things with white paper ”</p>
          </div>

          </div>
          <div class="col-sm-6 col-md-4">

          <div id="Victor"><img src="img/Victor.jpg" class="TeamUsImages"><p class="Names">Victor</p>
          <p class="Developer">Developer:<em> < Frond-End and Back-End Developer /> </em></p>
          <p class="SomethingAboutMe">About Me</p>
          <div class="SomethingAboutMeBorder"></div>
          <p class="AboutMe"></p>
          </div>

          </div>
          </div>
        </div>
        <div id="Sexth">
         
        </div>
        <div id="Seventh" class="container">
         <p class="ContactUS">Contact us</p>
          <div class="ContactUSBorder"></div>
          <form id="ContactUsForm" class="form-horizontal" role="form" method="post" action="php/ContactUs.php">
          <div class="row">
          <div class="ContactUsFormInputsDivName">
            <input type="text" placeholder="Name" name="name" class="Inputs" id="name">
          </div>
          <div class="ContactUsFormInputsDivEmail">
            <input type="email" placeholder="Email" name="email" class="Inputs" id="email">
          </div>
          </div>
          <div class="formtextarea">
          <textarea placeholder="Notes" name="message" id="message" class="InputsTextArea" style="margin: 0px; width: 100%; height: 567px;"></textarea>
          </div>
          <div class="ButtonHolder">
            <input type="submit" value="Submit" name="submit" id="button">
          </div>
          </form>
        </div>
        <script type="text/javascript">var br = 0;</script>
  </body>
</html>
